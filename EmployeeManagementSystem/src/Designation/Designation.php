<?php

namespace Sandwitch\Designation;

use Sandwitch\Common\Common;
use Sandwitch\Db\Connection;
use Sandwitch\Logger\notifier;
use Sandwitch\Utility\Utility;
use PDO;

class Designation implements Common
{
    public $notifier;
    public function __construct(notifier $obj)
    {
        $this->notifier = $obj;
    }

    public function add($data)
    {
        if (is_null($data)){
            return;
        }
        $name = $data['name'];
        $created = 'admin';
        $created_date = 'NOW';
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->prepare("INSERT INTO `employee_designations` (`title`,`created_by`, `create_date`) 
              VALUES (:department, :crated, :date_by)");

        $stm->bindParam(':department', $name, PDO::PARAM_STR);
        $stm->bindParam(':crated', $created, PDO::PARAM_STR);
        $stm->bindParam(':date_by', $created_date, PDO::PARAM_STR);
        $result = $stm->execute();

        if($result) {
            $msg = $this->notifier->setMessage(" Has been successfully Created");
            Utility::setMessage($msg);
            Utility::redirect("../../views/designation/designation.php");

        } else {
            $msg = $this->notifier->setMessage("There is some error.Please try again.");
            Utility::setMessage($msg);
            Utility::redirect("../../views/designation/designation.php");
        }
    }

    public function getAll()
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->query("SELECT * FROM `employee_designations` ORDER BY id DESC ");
        $designation = $stm->fetchAll(PDO::FETCH_OBJ);
        return $designation;
    }
    public function show($id)
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->prepare("SELECT * FROM `employee_designations` WHERE id = :id");
        $stm->execute(array(':id' => $id));
        $designation = $stm->fetchAll(PDO::FETCH_OBJ);
        return $designation;
    }

    public function edit($data)
    {
        if (is_null($data)){
            return;
        }
        $con = new Connection();
        $db = $con->openConnection();
        $designationTitle = $data['name'];
        $id = $data['id'];
        $modified_by = 'admin';
        $time = 'NOW()';
        $stm = $db->prepare("UPDATE `employee_designations` SET `title` = ?, `modified_by` = ?,`modified_date` = ? WHERE `employee_designations`       .`id` = ? ");
        $stm->bindValue(1, $designationTitle, PDO::PARAM_STR);
        $stm->bindValue(2, $modified_by, PDO::PARAM_STR);
        $stm->bindValue(3, $time, PDO::PARAM_STR);
        $stm->bindValue(4, $id, PDO::PARAM_INT);
        $stm->execute();
        $affected_rows = $stm->rowCount();

        if($affected_rows) {
            $msg = $this->notifier->setMessage(" Has been successfully Updated");
            Utility::setMessage($msg);
            Utility::redirect("../../views/designation/designationHistory.php");

        } else {
            $msg = $this->notifier->setMessage("There is some error.Please try again.");
            Utility::setMessage($msg);
            Utility::redirect("../../views/designation/designationHistory.php");
        }
    }

    public function delete($id)
    {
        if (is_null($id)){
            return;
        }
        $con = new Connection();
        $db = $con->openConnection();
        $stm=$db->prepare("DELETE FROM `employee_designations` WHERE id = :id");
        $stm->bindParam(':id', $id, PDO::PARAM_INT);
        $stm->execute();
        $affected_rows = $stm->rowCount();

        if($affected_rows) {
            $msg = $this->notifier->setMessage(" Has been successfully Deleted");
            Utility::setMessage($msg);
            Utility::redirect("../../views/designation/designationHistory.php");

        } else {
            $msg = $this->notifier->setMessage("There is some error.Please try again.");
            Utility::setMessage($msg);
            Utility::redirect("../../views/designation/designationHistory.php");
        }

    }

    public function getDesignationByEmployee($id)
    {
        if (is_null($id)){
            return;
        }
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->prepare("SELECT employee_designations.title FROM `employee_designations` INNER JOIN                                              employees ON employees.designation_id = employee_designations.id WHERE employees.id= :id");
        $stm->execute(array(':id' => $id));
        $designation = $stm->fetch(PDO::FETCH_OBJ);
        return $designation;
    }

    public function total()
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->prepare("SELECT * FROM `employee_designations`");
        $stm->execute();
        $departsments = $stm->rowCount();
        return $departsments;
    }

}