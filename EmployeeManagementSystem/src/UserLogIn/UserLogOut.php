<?php

namespace Sandwitch\UserLogIn;

use Sandwitch\Utility\Utility;


class UserLogOut
{
    static public function logOut()
    {
        session_start();
        unset($_SESSION['userName']);
        session_destroy();
        Utility::redirect('../../users/index.php');

    }

}