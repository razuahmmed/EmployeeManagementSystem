<?php
namespace Sandwitch\Login;

use Sandwitch\Utility\Utility;

class Logout
{
    static public function logOut()
    {
        session_start();
        unset($_SESSION['userName']);
        session_destroy();
        Utility::redirect('../../index.php');

    }

}


