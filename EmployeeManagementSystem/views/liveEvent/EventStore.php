<?php
require_once "../../vendor/autoload.php";

use Sandwitch\Event\Event;
use Sandwitch\Logger\EventMessage;

$eventMessage = new EventMessage();
$event = new Event($eventMessage);
$event->add($_POST);
