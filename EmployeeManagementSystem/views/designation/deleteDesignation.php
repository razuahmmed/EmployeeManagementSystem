<?php

require_once "../../vendor/autoload.php";

use Sandwitch\Designation\Designation;
use Sandwitch\Logger\DesignationMessage;

$designationMessage = new DesignationMessage();
$obj = new Designation($designationMessage);
$designation = $obj->delete($_GET['id']);