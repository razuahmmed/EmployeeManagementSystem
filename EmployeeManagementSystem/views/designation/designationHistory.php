<?php
require_once "../../vendor/autoload.php";

use Sandwitch\Designation\Designation;
use Sandwitch\Logger\DesignationMessage;

$designationMessage = new DesignationMessage();
$designation = new Designation($designationMessage);

$designations = $designation->getAll();

?>
<?php
$message = \Sandwitch\Utility\Utility::flushMessage();
?>


<!--header-->
         <?php require_once "../elements/header.php"?>
    <!--header-->

    <body class="nav-md">
    <div class="container body">
        <div class="main_container">

            <!-- side and top bar include -->
            <?php include '../elements/nav.php' ?>
            <!-- /side and top bar include -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Designations History</h3>

                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <div class="page-title">

                                        <?php
                                        if (!is_null($message)){
                                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                                            echo $message;
                                            echo " </div>";
                                        }

                                        ?>
                                    </div>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <p class="text-muted font-13 m-b-30">

                                    </p>

                                    <table id="datatable-buttons" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Designation Title</th>
                                            <th>Created_by</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($designations as $designation){
                                            ?>
                                            <tr>
                                                <td><?=$designation->title; ?></td>
                                                <td><?=$designation->created_by; ?></td>
                                                <td>
                                                    <?php
                                                    if($designation->status == 1){
                                                        echo "Active";
                                                    }else{
                                                        echo "InActive";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary" title="show" href="showDesignation.php?id=<?php echo $designation->id; ?>">
                                                        <span class="glyphicon glyphicon-education"></span>
                                                    </a>
                                                    <a class="btn btn-success" title="edit" href="editDesignation.php?id=<?php echo $designation->id; ?>">
                                                        <span class="glyphicon glyphicon-edit"></span>
                                                    </a>
                                                    <a class="btn btn-danger" title="delete" href="deleteDesignation.php?id=<?php echo$designation->id; ?>">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            <!-- footer content include -->
            <?php include '../elements/footer.php' ?>
            <!-- /footer content include -->
        </div>
    </div>

    <!--script-->
        <?php require_once "../elements/script.php"?>
    <!--script-->
    </body>
    </html>
